def main():
    arr = [69, 81, 30, 38, 9, 2, 47, 61, 32, 79]
    
    for i, v in enumerate(arr):
        min_i = i
        for i2, v2 in enumerate(arr[i+1:], i+1):
            if arr[min_i] > v2:
                min_i = i2
        arr[i], arr[min_i] = arr[min_i], arr[i]
        print(arr)

    print(arr)


if __name__ == "__main__":
    main()
