#include <iostream>

using namespace std;

int main() {
  int arr[10] = {69, 81, 30, 38, 9, 2, 47, 61, 32, 79};
  int min_i, tmp;

  for (int i=0; i<(sizeof(arr)/sizeof(*arr)); i++) {
    min_i = i;
    tmp = 0;

    for (int j=i+1; j<(sizeof(arr)/sizeof(*arr)); j++) {
      if (arr[min_i] > arr[j])
	min_i = j;
    }

    tmp = arr[i];
    arr[i] = arr[min_i];
    arr[min_i] = tmp;

    for (int i=0; i<(sizeof(arr)/sizeof(*arr)); i++) {
      cout << arr[i] << " ";
    }
    cout << "\n";
  }

  for (int i=0; i<(sizeof(arr)/sizeof(*arr)); i++) {
    cout << arr[i] << " ";
  }
  cout << "\n";
}
